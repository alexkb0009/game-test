﻿/// <reference path="Character.ts" />

module Character {

    export interface StaticStats {

        combat: {
            strength: number,
            accuracy: number,
            selfConfidence: number,
            healthPoints: number,
            speed: number,
        }

    }

    export interface VariableStats {

        confidence: number,
        healthPoints: number

    }

    /**
     * This is mostly a read-only class, with data fetched from and updated on server - to prevent cheating etc.
     */
    export class CharacterStats {

        character: Character;

        /* Static skills that must be levelled up */
        static: StaticStats = {
            combat: {
                strength: 1,
                accuracy: 1,
                selfConfidence: 1,
                healthPoints: 10,
                speed: 1
            }
        };

        /* Variable skills that change more rapidly (e.g. can be recharged) */
        variable: VariableStats = {
            confidence: 1,
            healthPoints: this.static.combat.healthPoints
        }

        combatLevel() {
            var sum: number = 0, count: number = 0;
            for (let combatSkill in this.static.combat) {
                sum += this.static.combat[combatSkill];
                count++;
            }
            return Math.round(sum / count);
        }


        constructor(parent: Character, staticStats: StaticStats = null, variableStats: VariableStats = null) {
            this.character = parent;
            if (staticStats) this.static = staticStats;
            if (variableStats) this.variable = variableStats;
            if (this.character.isPlayer) this.fetchStats();
        }

        /* ToDo: Bind to server request, then update stats with results */
        fetchStats() {

            // If this.character.isPlayer, ajax '/api/user/' + this.character.name + '/stats' or (or use websocket).

            // Else, is new account/player.
        }

    }

}
