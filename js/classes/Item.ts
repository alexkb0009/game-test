﻿/// <reference path="../libraries/jquery.d.ts" />
/// <reference path="Character.ts" />

module Items {

    /* Items which may be held in a player's inventory */
    export class Item {

        name: string = "Mysterious Item";

        equippable: boolean = false;
        
        constructor(name: string) {
            this.name = name;
        }

    }

    /* Items which can be equipped by players, also */
    export class EquippableItem extends Item {

        equippable: boolean = true;

        equippableLocations: Array<Character.BodyPart>; /* Some items are expected to take up multiple slots, e.g. large weapons. */

        addedStats: Character.StaticStats;

        constructor(name: string, equippableLocations: Array<Character.BodyPart>, addedStats: Character.StaticStats) {
            super(name);
            this.equippableLocations = equippableLocations;
            this.addedStats = addedStats;
        }

    }

    /* Items which can be consumed by players, such as food & drink */
    export class ConsumableItem extends Item {

        equippable: boolean = false;

        addedStatsOnConsumption: {
            static: Character.StaticStats,
            variable: Character.VariableStats
        };

        durationOfAddedStats: number; /* In milliseconds */

        consume(character: Character.Character) {
            if (!character.stats) return false;

            function updateCharacterStats(stats, addedStats){
                $.each(stats, (index: any, stat: any) => {
                    if ($.isArray(stat)) {
                        updateCharacterStats(stat, addedStats[index]);
                        return;
                    }
                    stat = stat + addedStats[index];
                });
            }

            updateCharacterStats([character.stats.static, character.stats.variable], this.addedStatsOnConsumption);
            
        }

        constructor(name: string, addedStatsOnConsumption: { static: Character.StaticStats, variable: Character.VariableStats }, duration: number = 30 * 1000 ) {
            super(name);
            this.durationOfAddedStats = duration;
            this.addedStatsOnConsumption = addedStatsOnConsumption;
        }

    }

}