﻿/// <reference path="UI.ts" />
/// <reference path="Character.ts" />

module Game {

    /* Singleton class for game state & functions */

    export class Game {

        title: string = "Game Test";

        me: Character.Me;

        scene: Scene;

        constructor(me: Character.Me) {
            this.me = me;
            this.scene = new Scene(this, me);
        }

        currentNPCs = new Array<Character.NPC>();

        static startGame(me: Character.Me = null, name: string = null, callback: Function = null) {
            if (!me) {
                if (!name) name = "Guest";
                me = new Character.Me(name = name);
            }

            // ToDo: authenticate name (+ password?) in Character.Me constructor (?) 

            if (me) UI.UI.game = new Game(me);
            callback();
        }

    }

}