﻿/// <reference path="Game.ts" />
// <reference path="3d/materials/raw/babylon.waterMaterial.tsn" />

module Game {

    export interface SceneInterface {

        scene: BABYLON.Scene;
        mainCamera: BABYLON.FreeCamera;
        materials: MaterialLibrary;

        sunlight: BABYLON.SpotLight;
        ground: BABYLON.GroundMesh;
        groundLevel: number;
        extraGround: Array<BABYLON.GroundMesh>;
        shadowGenerator: BABYLON.ShadowGenerator;
        environmentBox: BABYLON.Mesh;

        objects;
        actions;
        animations;

        startRendering();

    }


    /* Scene Base */

    export class SceneBase implements SceneInterface {

        scene = new BABYLON.Scene(UI.UI.engine);
        mainCamera: BABYLON.FreeCamera;
        materials: MaterialLibrary;

        sunlight: BABYLON.SpotLight;
        ground;
        groundLevel = 0;
        extraGround = new Array<BABYLON.GroundMesh>();
        shadowGenerator;
        environmentBox;

        objects;
        actions = {};
        animations = {};

        constructor(game: Game = UI.UI.game, me: Character.Me = game.me) {
            me.currentScene = this;
            me.moving = false;

            // Setup common elements (skybox, etc.)
            this.commonElements(me);

            // Bind UI controls to game (up/down/left/right keys, mouse-camera, etc.)
            UI.UI.setupGameBindings(game, this);

            // Render once even if renderloop not active.
            setTimeout(() => {
                UI.UI.engine.hideLoadingUI();
                this.scene.render();
            }, 1500);

        }

        startRendering() {
            /* Start rendering */
            UI.UI.engine.runRenderLoop(() => {
                if (!UI.UI.windowActive || UI.UI.mouse.y > 1) return false;
                if (
                    (this.mainCamera.rotation.x < 1 && UI.UI.mouse.y < 0) ||
                    (this.mainCamera.rotation.x > -1 && UI.UI.mouse.y > 0)
                ) {
                    /* Ensure camera doesn't over-rotate */
                    this.mainCamera.rotation.x += (-UI.UI.mouse.y * Math.abs(UI.UI.mouse.y)) / 20;
                }

                this.mainCamera.rotation.y += (UI.UI.mouse.x * (UI.UI.game.me.moving ? 1 : Math.abs(UI.UI.mouse.x) ) ) / (UI.UI.game.me.moving ? 40 : 20);

                this.scene.render();
            });

            return this;
        }

        commonElements(me: Character.Me = UI.UI.game.me) {

            BABYLON.SceneOptimizerOptions.HighDegradationAllowed(40);

            /* Materials Library */
            this.materials = new MaterialLibrary(this);

            /* Scene environment */
            this.scene.clearColor = new BABYLON.Color3(0.5, 0.6, 0.75);
            this.scene.ambientColor = new BABYLON.Color3(0.5, 0.5, 0.5);
            this.scene.gravity = new BABYLON.Vector3(0, -10, 0);
            this.scene.fogMode = BABYLON.Scene.FOGMODE_EXP2;
            this.scene.fogEnabled = true;
            this.scene.fogDensity = 0.02;
            this.scene.fogColor.r = 0.5;
            this.scene.fogColor.g = 0.6;
            this.scene.fogColor.b = 0.9;


            /* Camera */

            this.mainCamera = new BABYLON.FreeCamera("mainCamera", new BABYLON.Vector3(me.coordinates.x, this.groundLevel + 4, me.coordinates.z), this.scene);
            this.mainCamera.maxZ = 256;
            this.mainCamera.ellipsoid = new BABYLON.Vector3(0.5, 1.5, 0.5);
            this.mainCamera.checkCollisions = true;
            

            this.mainCamera.applyGravity = true;
            //this.mainCamera.setPhysicsState({ imposter: BABYLON.PhysicsEngine.SphereImpostor, mass: 0 });
            //this.mainCamera.attachControl(UI.UI.canvasRaw, false);
            me.camera = this.mainCamera;
            this.mainCamera.setEnabled(true);

            /* Environment Box */
            this.environmentBox = BABYLON.Mesh.CreateBox('environmentBox', 100, this.scene);
            this.environmentBox.infiniteDistance = true;

            return this;
        }

        setupShadows(
            objects: any = this.objects, /* An array or object*/
            shadowTargets: Array<any> = this.extraGround.concat(this.ground),
            light: BABYLON.IShadowLight = this.sunlight
        ) {
            if (objects == null) return;
            if (!this.shadowGenerator) {
                this.shadowGenerator = new BABYLON.ShadowGenerator(1024, light);
                this.shadowGenerator.useVarianceShadowMap = true;
                this.shadowGenerator.bias = 0.01;
                this.shadowGenerator.setDarkness(0.5);
            }

            var shadowGenerator = this.shadowGenerator;

            function recursiveAddToShadowGenerator(item, type: string) {
                console.log(item);
                if (!$.isArray(item) && !$.isPlainObject(item)) {
                    if (type == 'renderlist') {
                        shadowGenerator.getShadowMap().renderList.push(item);
                    }
                    if (type == 'shadowtargets') {
                        item.receiveShadows = true;
                    }
                    return;
                } else {
                    $.each(item, (subObjkey, subObjVal) => {
                        recursiveAddToShadowGenerator(item[subObjkey], type);
                    });
                }
            }

            recursiveAddToShadowGenerator(objects, 'renderlist');
            recursiveAddToShadowGenerator(shadowTargets, 'shadowtargets');

            return this;
        }

        destruct(placeholderText: string = 'Reconstructing Level') {

            /* Ensure complete dereferencing to minimize risk of eventual memory overload */

            this.ground.dispose();
            this.mainCamera.dispose();
            this.sunlight.dispose();
            this.shadowGenerator.dispose();

            while (this.extraGround.length) {
                this.extraGround[0].dispose();
                delete this.extraGround.pop();
            }

            $.each(this.objects, (i, o) => { o.dispose(); });

            delete this.actions,
            this.animations,
            this.objects,
            this.extraGround,
            this.ground,
            this.mainCamera,
            this.sunlight,
            this.shadowGenerator,
            this.materials;

            this.scene.dispose();
            delete this.scene;

            //UI.UI.engine.clear(new BABYLON.Color3(0, 0, 0), false, false);
            UI.UI.engine.stopRenderLoop();
            UI.UI.engine.flushFramebuffer();
            UI.UI.engine.loadingUIText = placeholderText;
            UI.UI.engine.displayLoadingUI();


            delete this;
        }

    }



    /* Default Implementation of a scene */

    export class Scene extends SceneBase {

        groundLevel = 2;

        /* Scene's non-common objects */

        objects: {
            sphere: BABYLON.AbstractMesh;
            water: BABYLON.Mesh;
            waterGround: BABYLON.Mesh;
            buildings: Array<BABYLON.AbstractMesh>;
            meshCopies: {
                bldg_instances?: Array<Array<BABYLON.AbstractMesh>>,
                bldg_components?: {},
                objects?: {}
            };
        };

        constructor(game: Game = UI.UI.game, me: Character.Me = game.me) {
            super(game, me);

            // Build up scene's terrain, objects, etc.
            this.setupEnvironment(me)
                .setupDefaultScene(me)
                .setupShadows(
                    [this.objects.sphere, this.objects.buildings/*, this.objects.meshCopies.bldg_instances*/],
                    this.extraGround.concat(this.ground, this.objects.buildings)
                )
                .startRendering();

        }

        setupEnvironment(me: Character.Me = UI.UI.game.me) {

            /* Scene */
            this.scene.enablePhysics(this.scene.gravity);
            this.scene.collisionsEnabled = true;

            /* Light */
            this.sunlight = new BABYLON.SpotLight('sunlight', new BABYLON.Vector3(10, 256, 10), new BABYLON.Vector3(0, -10, 0), 120, 2, this.scene);
            this.sunlight.setDirectionToTarget(new BABYLON.Vector3(0,0,0));
            this.sunlight.position.y = 512;

            /* Environment Box */
            this.environmentBox.material = this.materials.environment.skybox;

            return this;
        }
        

        setupDefaultScene(me?: Character.Me) {
            
            this.objects = {
                sphere : BABYLON.Mesh.CreateSphere('sphere1', 16, 4, this.scene),
                water: BABYLON.Mesh.CreateGround("water", 512, 512, 1, this.scene, true),
                waterGround: BABYLON.Mesh.CreateGround("waterGround", 512, 512, 1, this.scene),
                buildings: new Array<BABYLON.Mesh>(),
                meshCopies: null
            };


            /* Primary Ground */

            this.ground = BABYLON.Mesh.CreateGround('ground', 258, 258, 1, this.scene, true);
            this.ground.position = new BABYLON.Vector3(0, 2, 144); /* Left edge: -128, right edge: 128, top edge: 272, bottom edge: 16 */
            this.ground.material = this.materials.environment.terrain.sandGrassConcrete; //this.materials.environment.sand;
            this.ground.setPhysicsState(BABYLON.PhysicsEngine.PlaneImpostor, { mass: 0, friction: 1, restitution: 0.4 });
            this.ground.checkCollisions = true;

            /* Secondary Ground : Beaches, Shore */

            this.extraGround.push(BABYLON.Mesh.CreateGroundFromHeightMap("BottomBeach", "textures/heightmap/BeachMap_5x1.jpg", 320, 64, 64, -2, 2.01, this.scene));
            this.extraGround[0].position.z = -16;
            this.extraGround[0].position.x = 32; /* Left edge: -128 , Right edge: 192 */
            this.extraGround[0].material = this.materials.environment.sand5X1;
            this.extraGround[0].setPhysicsState(BABYLON.PhysicsEngine.HeightmapImpostor, { mass: 0, friction: 1, restitution: 1 });
            this.extraGround[0].checkCollisions = true;

            

            this.extraGround.push(BABYLON.Mesh.CreateGroundFromHeightMap("RightBeach", "textures/heightmap/BeachMap_1x4.jpg", 64, 256, 64, -2, 2.01, this.scene));
            this.extraGround[1].position.z = 144;
            this.extraGround[1].position.x = 160; /* Left edge: 128, right edge: 192 */
            this.extraGround[1].material = this.materials.environment.sand1X4;
            this.extraGround[1].checkCollisions = true;

            this.objects.waterGround.material = this.materials.environment.sand;
            this.objects.waterGround.position.y = -2;
            
            
            /* Sphere */

            this.objects.sphere.position.y = 12;
            this.objects.sphere.position.z = 25;
            //this.materials.metal.steel.wireframe = true;
            this.objects.sphere.material = this.materials.matte.concrete;
            //this.objects.sphere.applyGravity = true;
            this.objects.sphere.setPhysicsState(BABYLON.PhysicsEngine.SphereImpostor, { mass: 10, restitution: 1.75, friction: 0 });
            this.objects.sphere.checkCollisions = true;

            /* Add select objects to water render list */

            this.materials.environment.water.addToRenderList(this.environmentBox);
            this.materials.environment.water.addToRenderList(this.extraGround[0]);
            this.materials.environment.water.addToRenderList(this.extraGround[1]);
            this.materials.environment.water.addToRenderList(this.objects.waterGround);
            this.objects.water.material = this.materials.environment.water;

            /* Buildings */

            /* 1 - Small purposeless building - few instances of it */
            BABYLON.SceneLoader.ImportMesh(["Building_Small_1"], "meshes/buildings/", "building.small.1.babylon", this.scene, (meshes, particleSystems) => {
                /*
                $.each(meshes, (i) => {
                    meshes[i].material.checkReadyOnlyOnce = false;
                });
                */
                // 4 Meshes are added, but position of the first (buildings[0]) controls the other 3, as it is their parent mesh.
                this.objects.buildings = this.objects.buildings.concat(meshes);
                
                // Top-most Parent Node
                meshes[0].position.x = -64.20;
                meshes[0].position.z = 142;
                meshes[0].position.y = this.groundLevel;

                // Building Walls - also a parent node
                meshes[1].material = this.materials.matte.redbrick;

                // Window Sill (Top Right)
                meshes[2].material = this.materials.matte.concrete;

                // Door Awning
                meshes[3].material = this.materials.matte.concrete;

                // Upper Area
                meshes[4].material = this.materials.matte.concrete;

                // Foundation
                meshes[5].material = this.materials.matte.concrete;


                this.objects.meshCopies = {

                    bldg_instances: [
                        [
                            meshes[0].createInstance('Building_Small_1_Instance_2'),        // 0  Parent Node
                            meshes[1].createInstance('Building_Walls_Instance_2'),          // 1  - Parent Node
                            meshes[2].createInstance('WindowSills_Instance_2'),             // 2    --
                            meshes[3].createInstance('DoorAwning_Instance_2'),              // 3    --
                            meshes[4].createInstance('Building_Upper_Area_Instance_2'),     // 4  --
                            meshes[5].createInstance('Building_Foundation_Instance_2')      // 5  --
                        ],
                        [
                            meshes[0].createInstance('Building_Small_1_Instance_3'),        // 0  Parent Node
                            meshes[1].createInstance('Building_Walls_Instance_3'),          // 1  - Parent Node
                            meshes[2].createInstance('WindowSills_Instance_3'),             // 2    --
                            meshes[3].createInstance('DoorAwning_Instance_3'),              // 3    --
                            meshes[4].createInstance('Building_Upper_Area_Instance_3'),     // 4 --
                            meshes[5].createInstance('Building_Foundation_Instance_3'),     // 5 --
                        ],
                        [
                            meshes[0].createInstance('Building_Small_1_Instance_4'),        // 0  Parent Node
                            meshes[1].createInstance('Building_Walls_Instance_4'),          // 1  - Parent Node
                            meshes[2].createInstance('WindowSills_Instance_4'),             // 2    --
                            meshes[3].createInstance('DoorAwning_Instance_4'),              // 3    --
                            meshes[4].createInstance('Building_Upper_Area_Instance_4'),     // 4 --
                            meshes[5].createInstance('Building_Foundation_Instance_4'),     // 5 --
                        ]
                    ]

                };

                

                /** 
                 *  1st copied instance
                 */

                // Window sill & door awning
                this.objects.meshCopies.bldg_instances[0][2].parent = 
                this.objects.meshCopies.bldg_instances[0][3].parent = this.objects.meshCopies.bldg_instances[0][1];

                // Foundation & Upper Area
                this.objects.meshCopies.bldg_instances[0][1].parent = 
                this.objects.meshCopies.bldg_instances[0][4].parent =
                this.objects.meshCopies.bldg_instances[0][5].parent = this.objects.meshCopies.bldg_instances[0][0];

                this.objects.meshCopies.bldg_instances[0][0].position = meshes[0].position.clone();
                this.objects.meshCopies.bldg_instances[0][0].position.x = -78.5;
                this.objects.meshCopies.bldg_instances[0][0].position.z = 140;
                this.objects.meshCopies.bldg_instances[0][0].rotation.y = Math.PI / 2;

                /** 
                 *  2nd copied instance
                 */

                // Window sill & door awning
                this.objects.meshCopies.bldg_instances[1][2].parent =
                this.objects.meshCopies.bldg_instances[1][3].parent = this.objects.meshCopies.bldg_instances[1][1];

                // Foundation & Upper Area
                this.objects.meshCopies.bldg_instances[1][1].parent =
                this.objects.meshCopies.bldg_instances[1][4].parent =
                this.objects.meshCopies.bldg_instances[1][5].parent = this.objects.meshCopies.bldg_instances[1][0];

                this.objects.meshCopies.bldg_instances[1][0].position = this.objects.meshCopies.bldg_instances[0][0].position.clone();
                this.objects.meshCopies.bldg_instances[1][0].rotation = this.objects.meshCopies.bldg_instances[0][0].rotation.clone();
                this.objects.meshCopies.bldg_instances[1][0].position.z = 147.52;
                this.objects.meshCopies.bldg_instances[1][0].position.x = -78.51;
                
                /** 
                 *  3rd copied instance
                 */

                // Window sill & door awning
                this.objects.meshCopies.bldg_instances[2][2].parent =
                this.objects.meshCopies.bldg_instances[2][3].parent = this.objects.meshCopies.bldg_instances[2][1];

                // Foundation & Upper Area
                this.objects.meshCopies.bldg_instances[2][1].parent =
                this.objects.meshCopies.bldg_instances[2][4].parent =
                this.objects.meshCopies.bldg_instances[2][5].parent = this.objects.meshCopies.bldg_instances[2][0];

                this.objects.meshCopies.bldg_instances[2][0].position = this.objects.meshCopies.bldg_instances[1][0].position.clone();
                this.objects.meshCopies.bldg_instances[2][0].rotation.y = Math.PI * 1.5;
                this.objects.meshCopies.bldg_instances[2][0].position.z = 147.52;
                this.objects.meshCopies.bldg_instances[2][0].position.x = -94.5;

                this.setupShadows([meshes, this.objects.meshCopies.bldg_instances], meshes);

            });

            

            return this;
        }
        

    }

    

}