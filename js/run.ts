﻿/// <reference path="./classes/UI.ts" />
/// <reference path="./classes/Game.ts" />
/// <reference path="./classes/Character.ts" />

$(document).on('ready', () => {

    UI.UI.initialize();

    /* Introduction section interaction for login, etc. */

    var nameInput = $('#name_input_field'); 

    $('#intro_form').one('submit', (event) => {

        event.preventDefault();

        var name = nameInput.val();

        // Store name locally for faster future use
        if (typeof Storage !== 'undefined') {
            if (name) {
                localStorage.setItem('username', name);
            } else {
                localStorage.removeItem('username');
            }
        }

        Game.Game.startGame(
            null,
            name || "Guest",
            () => {
                setTimeout(() => {
                    $('#intro_container').remove();
                }, 1500);
            }
        );

        UI.UI.resizeCallback();

    });

    // Load last-entered username, if any.
    if (typeof Storage !== 'undefined') nameInput.val(localStorage.getItem('username'));

    // ToDo:
    // On input value change, wait 250ms (restart if change again), 
    // then query server for username ... 
    // (callback:) if user exists, show password field; else show create user form.

    // nameInput.on()

});

$(window).on('load', () => {

    UI.UI.documentBody.addClass('page-loaded');
    UI.UI.resizeCallback();

});