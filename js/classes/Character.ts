﻿/** 
 * Module containing classes for Characters
 */

module Character {

    interface CharacterInterface {

        isPlayer: boolean;              /* Whether character is a human (true) or non-playable character (NPC) (false). */

        name: string;                   /* Character's name */

        coordinates: BABYLON.Vector3;   /* Character's current location (within scene) */

        stats: CharacterStats;          /* Character's stats and skills */

        gear: CharacterGear;

        mesh?: BABYLON.AbstractMesh;

        currentScene?: Game.SceneBase;

        move(x: number, z: number);     /* Function for character to move. 
                                           x and z are for DIRECTION only, not coordinates. 
                                           Exact coordinates moved is product of direction x rate. */ 


    }

    interface CoordinateInterface {
        point: BABYLON.Vector3;
        scene?: string;
    }


    export class Character implements CharacterInterface {
        
        isPlayer: boolean;
        name: string;

        stats: CharacterStats = null;
        gear: CharacterGear = null;

        coordinates: BABYLON.Vector3 = new BABYLON.Vector3(0, 0, 0);
        
        mesh: BABYLON.Mesh; /* ToDo */

        updateRateOfTravel() {
            if (this.stats) this.cachedRate = (this.stats.static.combat.speed + 375) / 500;
            else this.cachedRate = 0.75;
        }

        cachedRate: number;
        
        updatedCoords(x: number, z: number) {
            /* X and Z are for direction, e.g. each should be 1 max. Need to ensure this on server. */
            var rate = this.cachedRate;

            if (x != 0 && z != 0) rate = Math.sqrt((rate * rate) / 2); 
            
            return {
                x: this.coordinates.x + x * rate,
                z: this.coordinates.z + z * rate
            };
        }

        animations = {
            moveCoordinates: new BABYLON.Animation("moveCoordinates", "position", 50, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT)
        };
        currentAnimation = null;

        animateMovement(
            animation: string,
            objects: Array<any>,
            oldCoords: BABYLON.Vector3,
            newCoords: BABYLON.Vector3,
            callback: () => any = null,
            duration: number = 1
        ) {
            var endFrame = duration * 25;
            var keys = [
                { frame: 0, value: oldCoords },
                { frame: endFrame, value: newCoords }
            ];

            if (!this.animations[animation].getKeys() || this.animations[animation].getKeys() != keys) {

                this.animations[animation].setKeys(keys);
            
                if (!(this.animations[animation] in objects[0].animations)) {

                    if (objects.length > 1) {
                        var i = 1;
                        while (i < objects.length) {
                            if (!(this.animations[animation] in objects[0].animations)) {
                                var animClone = this.animations[animation].clone();
                                objects[i].animations.push(animClone);
                                UI.UI.game.scene.scene.beginAnimation(objects[i], 0, endFrame, false, 10, () => {
                                    objects[i].animations.splice(objects[i].animations.indexOf(animClone), 1);
                                });
                            }
                            i++;
                        }
                    }
                
                    objects[0].animations.push(this.animations[animation]);
                    this.currentAnimation = UI.UI.game.scene.scene.beginAnimation(objects[0], 0, endFrame, false, 10, () => {
                        objects[0].animations.splice(objects[0].animations.indexOf(this.animations[animation]), 1);
                        callback();
                    });
                }
            }
        }

        /* Move by one movement unit in a direction specified by X & Z axes */

        move(x?: number, z?: number, direction?: { x: number, z: number }) {

            if ((!x || !z) && direction) {
                x = direction.x;
                z = direction.z;
            }
            
            /* ToDo: on server make sure x & z are always =< 1 */
            var updatedCoords = this.updatedCoords(x, z);
            updatedCoords.x += this.coordinates.x;
            updatedCoords.z += this.coordinates.z;

            if (this.mesh) {
                this.animateMovement(
                    'moveCoordinates',
                    [this.mesh],
                    new BABYLON.Vector3(this.mesh.position.x, this.mesh.position.y, this.mesh.position.z),
                    new BABYLON.Vector3(updatedCoords.x, this.mesh.position.y, updatedCoords.z),
                    null,
                    1
                );
            } 

            this.coordinates.x = updatedCoords.x;
            this.coordinates.z = updatedCoords.z;
            
        }
        
        constructor(name: string, staticStats: StaticStats = null, variableStats: VariableStats = null) {
            this.name = name;
            this.stats = new CharacterStats(this, staticStats, variableStats);
            this.updateRateOfTravel();
        }

    }


    


    export class Player extends Character {

        currentScene: Game.SceneBase;

        constructor(name: string) {
            super(name);
            this.isPlayer = true;
            this.stats = new CharacterStats(this);
            this.gear = new CharacterGear(this);
            this.fetchCoordinates();
        }

        fetchCoordinates() {
            /* ToDo - fetch from server, perhaps do more frequently as distance between other player & own character decreases */
        }

    }

    export class NPC extends Character {
        
        isFightable: boolean = false;

        constructor(name: string, isFightable: boolean, coordinates: BABYLON.Vector3 = null, staticStats: StaticStats = null, variableStats: VariableStats = null) {
            super(name, staticStats, variableStats);
            this.isPlayer = false;
            if (coordinates) this.coordinates = coordinates;
        }

    }

    export class Me extends Player {

        camera: BABYLON.FreeCamera;

        moving = false;

        updateCameraPosition() {
            this.camera.position.x = this.coordinates.x;
            this.camera.position.z = this.coordinates.z;
            return this;
        }

        updatedCoords(x: number, z: number) {
            /* X and Z are for direction, e.g. each should be 1 max. Need to ensure this on server. */
            var rate = this.cachedRate;
            var cosYRotation = Math.cos(this.camera.rotation.y);
            var sinYRotation = Math.sin(this.camera.rotation.y);

            /* If 2 axes are being moved on, use pythag theorem to reduce distance moved to retain fair speed/rate */
            if (x != 0 && z != 0) rate = Math.sqrt((rate * rate) / 2);
            
            return {
                x: (x * rate * cosYRotation) + (z * rate * sinYRotation),
                z: (z * rate * cosYRotation) - (x * rate * sinYRotation)
            };

            /* Older Version, for reference re: above code variant.
            var updatedX = this.coordinates.x, updatedZ = this.coordinates.y; 
            if (x != 0) {
                updatedX += x * rate * cosYRotation;
                updatedZ -= x * rate * sinYRotation;
            }

            if (z != 0) {
                updatedX += z * rate * sinYRotation;
                updatedZ += z * rate * cosYRotation;
            }
            return {x: updatedX, z: updatedZ };
            */
            
        }

        constructor(name: string, coordinates: BABYLON.Vector3 = null) {
            super(name);
            if (!coordinates) this.coordinates = new BABYLON.Vector3(0, 0, -10); /* ToDo - replace with syncCoordinates and have 0, 0, -10 be returned from there if not on server */
            else this.coordinates = coordinates;
        }

        move(x?: number, z?: number, direction?: { x: number, z: number }, continueMovement = false) {
            if (this.moving) return;
            this.moving = true;

            if ((!x || !z) && direction) {
                x = direction.x;
                z = direction.z;
            }

            var updatedCoords = this.updatedCoords(x, z);
            updatedCoords.x += this.coordinates.x;
            updatedCoords.z += this.coordinates.z;

            this.animateMovement(
                'moveCoordinates',
                [this.camera],
                this.camera.position,
                new BABYLON.Vector3(updatedCoords.x, this.camera.position.y, updatedCoords.z),
                () => {
                    this.moving = false;
                    if (continueMovement) {
                        // Continue movement recursively if set so.
                        var nextDirection = UI.UI.getCurrentControlsDirection();
                        if (nextDirection.x != 0 || nextDirection.z != 0 ) {
                            setTimeout(() => { this.move(null, null, nextDirection, true) }, 0);
                        }
                    }
                },
                UI.UI.controlsActive.extraFast ? 0.5 : 1 /* ToDo replace w/ mechanism that depletes energy */
            );

            this.coordinates.x = updatedCoords.x;
            this.coordinates.z = updatedCoords.z;


            //this.updateCameraCoordinate();
            this.syncCoordinates((result) => {
                if (result.position.x != this.coordinates.x || result.position.z != this.coordinates.z) {
                    this.coordinates.x = result.position.x;
                    this.coordinates.z = result.position.z;
                    //this.updateCameraPosition();
                }
            });
        }

        private throttleWait: boolean = false;

        throttledMove(direction: { x: number, z: number }, continueMovement = false) {
            if (!this.throttleWait) {
                this.move(null, null, direction, continueMovement);
                this.throttleWait = true;
                setTimeout(() => { this.throttleWait = false; }, 100);
            } else {
                return;
            }
        }


        syncCoordinates(callBack: Function) {
            
            /* ToDo - update coordinates on server */
            /* On server, verify that updated coordinates are within rate of travel ... */

        }





    }

    class Inventory extends Array {

        maxLength = 30;

        _push = Array.prototype.push;
        _concat = Array.prototype.concat;

        push(items) {
            var extraLength = 1;
            if ($.isArray(items)) extraLength = items.length;
            if (this.length + extraLength > this.maxLength) return this.length;

            return this._push(items);
        }

        concat(items) {
            if (this.length + items.length > this.maxLength) return;
            return this._concat(items);
        }

    }


}