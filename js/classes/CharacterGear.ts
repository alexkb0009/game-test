﻿/// <reference path="Character.ts" />
/// <reference path="Item.ts" />

module Character {

    export enum BodyPart {
        Head,
        Hair,
        Eyes,
        Body,
        ArmRight,
        ArmLeft,
        HandRight,
        HandLeft,
        Back,
        LegRight,
        LegLeft,
        Feet
    }

    export class CharacterGear {

        character: Character;

        equipped: Array<Items.EquippableItem>;

        inventory: Array<Items.Item>;

        constructor(parent: Character, equipped: Array<Items.EquippableItem> = null, inventory: Array<Items.Item> = null) {
            var validation = this.validate(equipped, inventory);
            if (validation.acceptable) {
                this.equipped = equipped;
                this.inventory = inventory;
            } else {

            }
            this.character = parent;
            if (this.character.isPlayer) this.fetchGear();

        }

        validate(equipped: Array<Items.EquippableItem> = null, inventory: Array<Items.Item> = null) {
            if (inventory && inventory.length > 30) return { acceptable: false, issue: "Not enough space in inventory." };
            if (equipped && equipped.length > 0) {
                var overlappingItem = null;
                var equippedSlots = new Array<BodyPart>();

                $.each(equipped, (index, item) => {
                    equippedSlots.concat(item.equippableLocations);
                });

                equippedSlots.sort();

                $.each(equippedSlots, (index, item) => {
                    if (equippedSlots[index + 1] == item) {
                        overlappingItem = item;
                        return;
                    }
                });

                if (overlappingItem) return { acceptable: false, issue: "Cannot wear another item in slot " + overlappingItem };
                
            }
            return {acceptable: true, issue: null};
            
        }

        fetchGear() {
            if (!this.character.isPlayer) return false;

        }
        


    }

}