var Game;
(function (Game) {
    var MaterialLibrary = (function () {
        function MaterialLibrary(scene) {
            this.scene = scene;
            this.createMaterials();
        }
        MaterialLibrary.prototype.createMaterials = function () {
            this.metal = {
                steel: new BABYLON.StandardMaterial('steel_1', this.scene.scene)
            };
            this.metal.steel.diffuseColor = new BABYLON.Color3(0.75, 0.75, 0.75);
            this.metal.steel.specularColor = new BABYLON.Color3(1.5, 1.5, 1.5);
            this.matte = {
                concrete: new BABYLON.StandardMaterial('concrete_1', this.scene.scene),
                redbrick: new BABYLON.StandardMaterial('redbrick_1', this.scene.scene)
            };
            this.matte.concrete.diffuseTexture = new BABYLON.Texture("./textures/concrete_1.jpg", this.scene.scene);
            this.matte.concrete.diffuseTexture.vScale = 1;
            this.matte.concrete.diffuseTexture.uScale = 1;
            this.matte.concrete.specularColor = new BABYLON.Color3(0.05, 0.05, 0.05);
            this.matte.concrete.ambientColor = new BABYLON.Color3(0.9, 0.9, 0.9);
            this.matte.redbrick.diffuseTexture = new BABYLON.Texture("./textures/redbrick_2.jpg", this.scene.scene);
            this.matte.redbrick.diffuseTexture.vScale = 1;
            this.matte.redbrick.diffuseTexture.uScale = 1;
            this.matte.redbrick.specularColor = new BABYLON.Color3(0, 0, 0);
            this.matte.redbrick.ambientColor = new BABYLON.Color3(1.2, 1.2, 1.2);
            this.environment = {
                skybox: new BABYLON.StandardMaterial('skybox', this.scene.scene),
                water: new BABYLON.WaterMaterial('water', this.scene.scene),
                sand: new BABYLON.StandardMaterial("sand", this.scene.scene),
                sand5X1: new BABYLON.StandardMaterial("sand5X1", this.scene.scene),
                sand1X4: new BABYLON.StandardMaterial("sand1X4", this.scene.scene),
                terrain: {
                    sandGrassConcrete: new BABYLON.TerrainMaterial("sandGrassConcrete1", this.scene.scene)
                }
            };
            this.environment.skybox.backFaceCulling = false;
            this.environment.skybox.diffuseColor = new BABYLON.Color3(0, 0, 0);
            this.environment.skybox.specularColor = new BABYLON.Color3(0, 0, 0);
            this.environment.skybox.reflectionTexture = new BABYLON.CubeTexture("./textures/skybox/skybox2", this.scene.scene);
            this.environment.skybox.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
            this.environment.water.backfaceCulling = true;
            this.environment.water.bumpTexture = new BABYLON.Texture("./textures/waterbump.png", this.scene.scene);
            this.environment.water.windForce = -15;
            this.environment.water.waveHeight = 0.25;
            this.environment.water.waveLength = 0.025;
            this.environment.water.waveSpeed = 0.4;
            this.environment.water.bumpHeight = 0.125;
            this.environment.sand.diffuseTexture = new BABYLON.Texture("textures/sand.jpg", this.scene.scene);
            this.environment.sand.diffuseTexture.vScale = 128;
            this.environment.sand.diffuseTexture.uScale = 128;
            this.environment.sand5X1.diffuseTexture = new BABYLON.Texture("textures/sand.jpg", this.scene.scene);
            this.environment.sand5X1.diffuseTexture.vScale = 32;
            this.environment.sand5X1.diffuseTexture.uScale = 160;
            this.environment.sand5X1.ambientColor = new BABYLON.Color3(1.8, 1.8, 1.8);
            this.environment.sand1X4.diffuseTexture = new BABYLON.Texture("textures/sand.jpg", this.scene.scene);
            this.environment.sand1X4.diffuseTexture.vScale = 128;
            this.environment.sand1X4.diffuseTexture.uScale = 32;
            this.environment.sand1X4.ambientColor = new BABYLON.Color3(1.8, 1.8, 1.8);
            this.environment.sand.specularColor =
                this.environment.sand5X1.specularColor =
                    this.environment.sand1X4.specularColor =
                        new BABYLON.Color3(0.1, 0.1, 0.1);
            this.environment.terrain.sandGrassConcrete.mixTexture = new BABYLON.Texture("textures/terrainmap/TerrainMap_1x1_BeachInner.png", this.scene.scene);
            this.environment.terrain.sandGrassConcrete.diffuseTexture1 = new BABYLON.Texture("textures/sand.jpg", this.scene.scene);
            this.environment.terrain.sandGrassConcrete.diffuseTexture1.vScale = 128;
            this.environment.terrain.sandGrassConcrete.diffuseTexture1.uScale = 128;
            this.environment.terrain.sandGrassConcrete.diffuseTexture2 = new BABYLON.Texture("textures/grass1_2.jpg", this.scene.scene);
            this.environment.terrain.sandGrassConcrete.diffuseTexture2.vScale = 48;
            this.environment.terrain.sandGrassConcrete.diffuseTexture2.uScale = 48;
            this.environment.terrain.sandGrassConcrete.diffuseTexture3 = new BABYLON.Texture("textures/stoneTile_1.jpg", this.scene.scene);
            this.environment.terrain.sandGrassConcrete.diffuseTexture3.vScale = 64;
            this.environment.terrain.sandGrassConcrete.diffuseTexture3.uScale = 64;
            this.environment.terrain.sandGrassConcrete.specularColor = new BABYLON.Color3(0.1, 0.1, 0.1);
            this.environment.terrain.sandGrassConcrete.ambientColor = new BABYLON.Color3(1.8, 1.8, 1.8);
        };
        return MaterialLibrary;
    })();
    Game.MaterialLibrary = MaterialLibrary;
})(Game || (Game = {}));
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Material;
(function (Material) {
    var Water = (function (_super) {
        __extends(Water, _super);
        function Water(name, scene, light) {
            var _this = this;
            _super.call(this, name, scene);
            this.actions = {};
            this.waterColor = new BABYLON.Color3(0.0, 0.3, 0.1);
            this.waterColorLevel = 0.2;
            this.fresnelLevel = 1;
            this.reflectionLevel = 0.6;
            this.refractionLevel = 0.8;
            this.waveLength = 0.1;
            this.waveHeight = 0.15;
            this.waterDirection = new BABYLON.Vector2(0, 1.0);
            this._time = 0;
            this.name = name;
            this.id = name;
            this.light = light;
            scene.materials.push(this);
            this.bumpTexture = new BABYLON.Texture("", scene);
            this.bumpTexture.uScale = 2;
            this.bumpTexture.vScale = 2;
            this.bumpTexture.wrapU = BABYLON.Texture.MIRROR_ADDRESSMODE;
            this.bumpTexture.wrapV = BABYLON.Texture.MIRROR_ADDRESSMODE;
            this.reflectionTexture = new BABYLON.MirrorTexture("reflection", 512, scene, true);
            this.reflectionTexture['mirrorPlane'] = new BABYLON.Plane(0, -1, 0, 0);
            this.refractionTexture = new BABYLON.RenderTargetTexture("refraction", 512, scene, true);
            this.getRenderTargetTextures = function () {
                var results = new BABYLON.SmartArray(2);
                results.push(_this.reflectionTexture);
                results.push(_this.refractionTexture);
                return results;
            };
            BABYLON.Engine.ShadersRepository = "";
        }
        Water.prototype.isReady = function (mesh) {
            var engine = this.getScene().getEngine();
            if (this.bumpTexture && !this.bumpTexture.isReady()) {
                return false;
            }
            this._effect = engine.createEffect("./classes/3D/Water", ["position", "normal", "uv"], ["worldViewProjection", "world", "view", "vLightPosition", "vEyePosition", "waterColor", "vLevels", "waveData", "windMatrix"], ["reflectionSampler", "refractionSampler", "bumpSampler"], "");
            if (!this._effect.isReady()) {
                return false;
            }
        };
        Water.prototype.bind = function (world, mesh) {
            this._time += 0.0001 * this.getScene().getAnimationRatio();
            this._effect.setMatrix("world", world);
            this._effect.setMatrix("worldViewProjection", world.multiply(this.getScene().getTransformMatrix()));
            this._effect.setVector3("vEyePosition", this.getScene().activeCamera.position);
            this._effect.setVector3("vLightPosition", this.light.position);
            this._effect.setColor3("waterColor", this.waterColor);
            this._effect.setFloat4("vLevels", this.waterColorLevel, this.fresnelLevel, this.reflectionLevel, this.refractionLevel);
            this._effect.setFloat2("waveData", this.waveLength, this.waveHeight);
            this._effect.setMatrix("windMatrix", this.bumpTexture.getTextureMatrix().multiply(BABYLON.Matrix.Translation(this.waterDirection.x * this._time, this.waterDirection.y * this._time, 0)));
            this._effect.setTexture("bumpSampler", this.bumpTexture);
            this._effect.setTexture("reflectionSampler", this.reflectionTexture);
            this._effect.setTexture("refractionSampler", this.refractionTexture);
        };
        Water.prototype.needAlphaBlending = function () { return false; };
        Water.prototype.needAlphaTesting = function () { return false; };
        Water.prototype.dispose = function () {
            this.dispose();
            if (this.bumpTexture) {
                this.bumpTexture.dispose();
            }
            if (this.reflectionTexture) {
                this.reflectionTexture.dispose();
            }
            if (this.refractionTexture) {
                this.refractionTexture.dispose();
            }
        };
        return Water;
    })(BABYLON.Material);
    Material.Water = Water;
})(Material || (Material = {}));
/**
 * Module containing classes for Characters
 */
var Character;
(function (Character_1) {
    var Character = (function () {
        function Character(name, staticStats, variableStats) {
            if (staticStats === void 0) { staticStats = null; }
            if (variableStats === void 0) { variableStats = null; }
            this.stats = null;
            this.gear = null;
            this.coordinates = new BABYLON.Vector3(0, 0, 0);
            this.animations = {
                moveCoordinates: new BABYLON.Animation("moveCoordinates", "position", 50, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT)
            };
            this.currentAnimation = null;
            this.name = name;
            this.stats = new Character_1.CharacterStats(this, staticStats, variableStats);
            this.updateRateOfTravel();
        }
        Character.prototype.updateRateOfTravel = function () {
            if (this.stats)
                this.cachedRate = (this.stats.static.combat.speed + 375) / 500;
            else
                this.cachedRate = 0.75;
        };
        Character.prototype.updatedCoords = function (x, z) {
            var rate = this.cachedRate;
            if (x != 0 && z != 0)
                rate = Math.sqrt((rate * rate) / 2);
            return {
                x: this.coordinates.x + x * rate,
                z: this.coordinates.z + z * rate
            };
        };
        Character.prototype.animateMovement = function (animation, objects, oldCoords, newCoords, callback, duration) {
            var _this = this;
            if (callback === void 0) { callback = null; }
            if (duration === void 0) { duration = 1; }
            var endFrame = duration * 25;
            var keys = [
                { frame: 0, value: oldCoords },
                { frame: endFrame, value: newCoords }
            ];
            if (!this.animations[animation].getKeys() || this.animations[animation].getKeys() != keys) {
                this.animations[animation].setKeys(keys);
                if (!(this.animations[animation] in objects[0].animations)) {
                    if (objects.length > 1) {
                        var i = 1;
                        while (i < objects.length) {
                            if (!(this.animations[animation] in objects[0].animations)) {
                                var animClone = this.animations[animation].clone();
                                objects[i].animations.push(animClone);
                                UI.UI.game.scene.scene.beginAnimation(objects[i], 0, endFrame, false, 10, function () {
                                    objects[i].animations.splice(objects[i].animations.indexOf(animClone), 1);
                                });
                            }
                            i++;
                        }
                    }
                    objects[0].animations.push(this.animations[animation]);
                    this.currentAnimation = UI.UI.game.scene.scene.beginAnimation(objects[0], 0, endFrame, false, 10, function () {
                        objects[0].animations.splice(objects[0].animations.indexOf(_this.animations[animation]), 1);
                        callback();
                    });
                }
            }
        };
        Character.prototype.move = function (x, z, direction) {
            if ((!x || !z) && direction) {
                x = direction.x;
                z = direction.z;
            }
            var updatedCoords = this.updatedCoords(x, z);
            updatedCoords.x += this.coordinates.x;
            updatedCoords.z += this.coordinates.z;
            if (this.mesh) {
                this.animateMovement('moveCoordinates', [this.mesh], new BABYLON.Vector3(this.mesh.position.x, this.mesh.position.y, this.mesh.position.z), new BABYLON.Vector3(updatedCoords.x, this.mesh.position.y, updatedCoords.z), null, 1);
            }
            this.coordinates.x = updatedCoords.x;
            this.coordinates.z = updatedCoords.z;
        };
        return Character;
    })();
    Character_1.Character = Character;
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(name) {
            _super.call(this, name);
            this.isPlayer = true;
            this.stats = new Character_1.CharacterStats(this);
            this.gear = new Character_1.CharacterGear(this);
            this.fetchCoordinates();
        }
        Player.prototype.fetchCoordinates = function () {
        };
        return Player;
    })(Character);
    Character_1.Player = Player;
    var NPC = (function (_super) {
        __extends(NPC, _super);
        function NPC(name, isFightable, coordinates, staticStats, variableStats) {
            if (coordinates === void 0) { coordinates = null; }
            if (staticStats === void 0) { staticStats = null; }
            if (variableStats === void 0) { variableStats = null; }
            _super.call(this, name, staticStats, variableStats);
            this.isFightable = false;
            this.isPlayer = false;
            if (coordinates)
                this.coordinates = coordinates;
        }
        return NPC;
    })(Character);
    Character_1.NPC = NPC;
    var Me = (function (_super) {
        __extends(Me, _super);
        function Me(name, coordinates) {
            if (coordinates === void 0) { coordinates = null; }
            _super.call(this, name);
            this.moving = false;
            this.throttleWait = false;
            if (!coordinates)
                this.coordinates = new BABYLON.Vector3(0, 0, -10);
            else
                this.coordinates = coordinates;
        }
        Me.prototype.updateCameraPosition = function () {
            this.camera.position.x = this.coordinates.x;
            this.camera.position.z = this.coordinates.z;
            return this;
        };
        Me.prototype.updatedCoords = function (x, z) {
            var rate = this.cachedRate;
            var cosYRotation = Math.cos(this.camera.rotation.y);
            var sinYRotation = Math.sin(this.camera.rotation.y);
            if (x != 0 && z != 0)
                rate = Math.sqrt((rate * rate) / 2);
            return {
                x: (x * rate * cosYRotation) + (z * rate * sinYRotation),
                z: (z * rate * cosYRotation) - (x * rate * sinYRotation)
            };
        };
        Me.prototype.move = function (x, z, direction, continueMovement) {
            var _this = this;
            if (continueMovement === void 0) { continueMovement = false; }
            if (this.moving)
                return;
            this.moving = true;
            if ((!x || !z) && direction) {
                x = direction.x;
                z = direction.z;
            }
            var updatedCoords = this.updatedCoords(x, z);
            updatedCoords.x += this.coordinates.x;
            updatedCoords.z += this.coordinates.z;
            this.animateMovement('moveCoordinates', [this.camera], this.camera.position, new BABYLON.Vector3(updatedCoords.x, this.camera.position.y, updatedCoords.z), function () {
                _this.moving = false;
                if (continueMovement) {
                    var nextDirection = UI.UI.getCurrentControlsDirection();
                    if (nextDirection.x != 0 || nextDirection.z != 0) {
                        setTimeout(function () { _this.move(null, null, nextDirection, true); }, 0);
                    }
                }
            }, UI.UI.controlsActive.extraFast ? 0.5 : 1);
            this.coordinates.x = updatedCoords.x;
            this.coordinates.z = updatedCoords.z;
            this.syncCoordinates(function (result) {
                if (result.position.x != _this.coordinates.x || result.position.z != _this.coordinates.z) {
                    _this.coordinates.x = result.position.x;
                    _this.coordinates.z = result.position.z;
                }
            });
        };
        Me.prototype.throttledMove = function (direction, continueMovement) {
            var _this = this;
            if (continueMovement === void 0) { continueMovement = false; }
            if (!this.throttleWait) {
                this.move(null, null, direction, continueMovement);
                this.throttleWait = true;
                setTimeout(function () { _this.throttleWait = false; }, 100);
            }
            else {
                return;
            }
        };
        Me.prototype.syncCoordinates = function (callBack) {
            /* ToDo - update coordinates on server */
            /* On server, verify that updated coordinates are within rate of travel ... */
        };
        return Me;
    })(Player);
    Character_1.Me = Me;
    var Inventory = (function (_super) {
        __extends(Inventory, _super);
        function Inventory() {
            _super.apply(this, arguments);
            this.maxLength = 30;
            this._push = Array.prototype.push;
            this._concat = Array.prototype.concat;
        }
        Inventory.prototype.push = function (items) {
            var extraLength = 1;
            if ($.isArray(items))
                extraLength = items.length;
            if (this.length + extraLength > this.maxLength)
                return this.length;
            return this._push(items);
        };
        Inventory.prototype.concat = function (items) {
            if (this.length + items.length > this.maxLength)
                return;
            return this._concat(items);
        };
        return Inventory;
    })(Array);
})(Character || (Character = {}));
/// <reference path="../libraries/jquery.d.ts" />
/// <reference path="Character.ts" />
var Items;
(function (Items) {
    var Item = (function () {
        function Item(name) {
            this.name = "Mysterious Item";
            this.equippable = false;
            this.name = name;
        }
        return Item;
    })();
    Items.Item = Item;
    var EquippableItem = (function (_super) {
        __extends(EquippableItem, _super);
        function EquippableItem(name, equippableLocations, addedStats) {
            _super.call(this, name);
            this.equippable = true;
            this.equippableLocations = equippableLocations;
            this.addedStats = addedStats;
        }
        return EquippableItem;
    })(Item);
    Items.EquippableItem = EquippableItem;
    var ConsumableItem = (function (_super) {
        __extends(ConsumableItem, _super);
        function ConsumableItem(name, addedStatsOnConsumption, duration) {
            if (duration === void 0) { duration = 30 * 1000; }
            _super.call(this, name);
            this.equippable = false;
            this.durationOfAddedStats = duration;
            this.addedStatsOnConsumption = addedStatsOnConsumption;
        }
        ConsumableItem.prototype.consume = function (character) {
            if (!character.stats)
                return false;
            function updateCharacterStats(stats, addedStats) {
                $.each(stats, function (index, stat) {
                    if ($.isArray(stat)) {
                        updateCharacterStats(stat, addedStats[index]);
                        return;
                    }
                    stat = stat + addedStats[index];
                });
            }
            updateCharacterStats([character.stats.static, character.stats.variable], this.addedStatsOnConsumption);
        };
        return ConsumableItem;
    })(Item);
    Items.ConsumableItem = ConsumableItem;
})(Items || (Items = {}));
/// <reference path="Character.ts" />
/// <reference path="Item.ts" />
var Character;
(function (Character) {
    (function (BodyPart) {
        BodyPart[BodyPart["Head"] = 0] = "Head";
        BodyPart[BodyPart["Hair"] = 1] = "Hair";
        BodyPart[BodyPart["Eyes"] = 2] = "Eyes";
        BodyPart[BodyPart["Body"] = 3] = "Body";
        BodyPart[BodyPart["ArmRight"] = 4] = "ArmRight";
        BodyPart[BodyPart["ArmLeft"] = 5] = "ArmLeft";
        BodyPart[BodyPart["HandRight"] = 6] = "HandRight";
        BodyPart[BodyPart["HandLeft"] = 7] = "HandLeft";
        BodyPart[BodyPart["Back"] = 8] = "Back";
        BodyPart[BodyPart["LegRight"] = 9] = "LegRight";
        BodyPart[BodyPart["LegLeft"] = 10] = "LegLeft";
        BodyPart[BodyPart["Feet"] = 11] = "Feet";
    })(Character.BodyPart || (Character.BodyPart = {}));
    var BodyPart = Character.BodyPart;
    var CharacterGear = (function () {
        function CharacterGear(parent, equipped, inventory) {
            if (equipped === void 0) { equipped = null; }
            if (inventory === void 0) { inventory = null; }
            var validation = this.validate(equipped, inventory);
            if (validation.acceptable) {
                this.equipped = equipped;
                this.inventory = inventory;
            }
            else {
            }
            this.character = parent;
            if (this.character.isPlayer)
                this.fetchGear();
        }
        CharacterGear.prototype.validate = function (equipped, inventory) {
            if (equipped === void 0) { equipped = null; }
            if (inventory === void 0) { inventory = null; }
            if (inventory && inventory.length > 30)
                return { acceptable: false, issue: "Not enough space in inventory." };
            if (equipped && equipped.length > 0) {
                var overlappingItem = null;
                var equippedSlots = new Array();
                $.each(equipped, function (index, item) {
                    equippedSlots.concat(item.equippableLocations);
                });
                equippedSlots.sort();
                $.each(equippedSlots, function (index, item) {
                    if (equippedSlots[index + 1] == item) {
                        overlappingItem = item;
                        return;
                    }
                });
                if (overlappingItem)
                    return { acceptable: false, issue: "Cannot wear another item in slot " + overlappingItem };
            }
            return { acceptable: true, issue: null };
        };
        CharacterGear.prototype.fetchGear = function () {
            if (!this.character.isPlayer)
                return false;
        };
        return CharacterGear;
    })();
    Character.CharacterGear = CharacterGear;
})(Character || (Character = {}));
/// <reference path="Character.ts" />
var Character;
(function (Character) {
    var CharacterStats = (function () {
        function CharacterStats(parent, staticStats, variableStats) {
            if (staticStats === void 0) { staticStats = null; }
            if (variableStats === void 0) { variableStats = null; }
            this.static = {
                combat: {
                    strength: 1,
                    accuracy: 1,
                    selfConfidence: 1,
                    healthPoints: 10,
                    speed: 1
                }
            };
            this.variable = {
                confidence: 1,
                healthPoints: this.static.combat.healthPoints
            };
            this.character = parent;
            if (staticStats)
                this.static = staticStats;
            if (variableStats)
                this.variable = variableStats;
            if (this.character.isPlayer)
                this.fetchStats();
        }
        CharacterStats.prototype.combatLevel = function () {
            var sum = 0, count = 0;
            for (var combatSkill in this.static.combat) {
                sum += this.static.combat[combatSkill];
                count++;
            }
            return Math.round(sum / count);
        };
        CharacterStats.prototype.fetchStats = function () {
            // If this.character.isPlayer, ajax '/api/user/' + this.character.name + '/stats' or (or use websocket).
        };
        return CharacterStats;
    })();
    Character.CharacterStats = CharacterStats;
})(Character || (Character = {}));
var UI;
(function (UI_1) {
    var UI = (function () {
        function UI() {
        }
        UI.getCurrentControlsDirection = function () {
            return {
                x: UI.controlsActive.right ? 1 : (UI.controlsActive.left ? -1 : 0),
                z: UI.controlsActive.down ? -1 : (UI.controlsActive.up ? 1 : 0)
            };
        };
        UI.canvasHeight = function () {
            return UI.currentState.window.height - UI.currentState.window.heightOffset;
        };
        UI.initialize = function () {
            UI.canvasContainer = $("#canvas_container");
            UI.documentBody = $("body");
            UI.topBar = UI.documentBody.find('#top_bar');
            UI.updateSizeVariables();
            var canvasHTML = '<canvas id="game_canvas" width="' + UI.currentState.document.width + '" height="' + UI.canvasHeight() + '"></canvas>';
            UI.canvasContainer.html(canvasHTML);
            UI.canvasRaw = document.getElementById('game_canvas');
            UI.canvas = UI.canvasContainer.children('#game_canvas');
            UI.engine = new BABYLON.Engine(UI.canvasRaw);
            UI.resizeCallback(true);
            $(window).on('resize', function () {
                setTimeout(UI.resizeCallback, 150);
            });
        };
        UI.setupUIBindings = function (game) {
            /* Mouse Coords */
            UI.mouse = new BABYLON.Vector2(0, 0);
            UI.documentBody.on('mousemove', function (event) {
                UI.mouse.x = (event.clientX / UI.currentState.document.width) * 2 - 1;
                UI.mouse.y = -((event.clientY - UI.currentState.window.heightOffset) / UI.canvasHeight()) * 2 + 1;
            });
            $(window).on('blur', function () { UI.windowActive = false; });
            $(window).on('focus', function () { UI.windowActive = true; });
            if (!UI.DOMBinding.documentBody)
                UI.DOMBinding.documentBody = rivets.bind(UI.documentBody, { game: game });
            $('.in-game-only').css('display', '');
            setTimeout(function () {
                UI.documentBody.addClass('ui-game-loaded');
            }, 250);
            $('#top_bar .resolution.menu > li > a').on('click', function () {
                var $this = $(this);
                switch ($this.text()) {
                    case 'High':
                        UI.engine.setHardwareScalingLevel(1);
                        break;
                    case 'Medium':
                        UI.engine.setHardwareScalingLevel(1.25);
                        break;
                    case 'Low':
                        UI.engine.setHardwareScalingLevel(1.5);
                        break;
                    case 'Very Low':
                        UI.engine.setHardwareScalingLevel(2);
                        break;
                    case '1992':
                        UI.engine.setHardwareScalingLevel(2.5);
                        break;
                    case '1989':
                        UI.engine.setHardwareScalingLevel(4);
                        break;
                }
                $this.parent().children('active').removeClass('active');
                $this.addClass('active');
            });
        };
        UI.setupGameBindings = function (game, scene) {
            if (scene === void 0) { scene = game.scene; }
            scene.scene.actionManager = new BABYLON.ActionManager(scene.scene);
            scene.actions['keydown'] = new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, function (event) {
                /* Move character (+ camera) via WASD buttons */
                switch (event.sourceEvent.keyCode) {
                    case 87:
                        UI.controlsActive.up = true;
                        break;
                    case 65:
                        UI.controlsActive.left = true;
                        break;
                    case 83:
                        UI.controlsActive.down = true;
                        break;
                    case 68:
                        UI.controlsActive.right = true;
                        break;
                    case 82:
                        UI.controlsActive.extraFast = true;
                        break;
                }
                if (!game.me.moving) {
                    game.me.move(null, null, UI.getCurrentControlsDirection(), true);
                }
            });
            scene.actions['keyup'] = new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, function (event) {
                switch (event.sourceEvent.keyCode) {
                    case 87:
                        UI.controlsActive.up = false;
                        break;
                    case 65:
                        UI.controlsActive.left = false;
                        break;
                    case 83:
                        UI.controlsActive.down = false;
                        break;
                    case 68:
                        UI.controlsActive.right = false;
                        break;
                    case 82:
                        UI.controlsActive.extraFast = false;
                        break;
                }
            });
            scene.scene.actionManager.registerAction(scene.actions['keydown']);
            scene.scene.actionManager.registerAction(scene.actions['keyup']);
            if (!UI.documentBody.hasClass('ui-game-loaded'))
                UI.setupUIBindings(game);
        };
        UI.updateSizeVariables = function () {
            UI.currentState.document.width = UI.documentBody.innerWidth();
            UI.currentState.document.height = UI.documentBody.innerHeight();
            UI.currentState.window.height = window.innerHeight;
            UI.currentState.window.width = window.innerWidth;
            UI.currentState.window.heightOffset = UI.topBar.outerHeight();
        };
        UI.resizeCallback = function (skipUpdateSizing) {
            if (skipUpdateSizing === void 0) { skipUpdateSizing = false; }
            if (!skipUpdateSizing)
                UI.updateSizeVariables();
            UI.canvas.height(UI.canvasHeight());
            UI.canvas.width(UI.currentState.window.width);
            UI.engine.resize();
            Utility.verticalCenterElements();
        };
        UI.windowActive = true;
        UI.currentState = {
            document: {
                width: 0,
                height: 0
            },
            window: {
                height: 0,
                heightOffset: 0,
                width: 0
            }
        };
        UI.controlsActive = {
            up: false,
            left: false,
            down: false,
            right: false,
            extraFast: false
        };
        UI.DOMBinding = {
            documentBody: null,
        };
        UI.ActionBinding = {};
        return UI;
    })();
    UI_1.UI = UI;
    var Messager = (function () {
        function Messager() {
        }
        Messager.messages = new Array();
        return Messager;
    })();
    UI_1.Messager = Messager;
    var ImportanceLevel;
    (function (ImportanceLevel) {
        ImportanceLevel[ImportanceLevel["Error"] = 0] = "Error";
        ImportanceLevel[ImportanceLevel["Important"] = 1] = "Important";
        ImportanceLevel[ImportanceLevel["Minor"] = 2] = "Minor";
        ImportanceLevel[ImportanceLevel["Player"] = 3] = "Player";
        ImportanceLevel[ImportanceLevel["Status"] = 4] = "Status";
        ImportanceLevel[ImportanceLevel["Dialog"] = 5] = "Dialog";
        ImportanceLevel[ImportanceLevel["Debug"] = 6] = "Debug";
    })(ImportanceLevel || (ImportanceLevel = {}));
    var Utility = (function () {
        function Utility() {
        }
        Utility.getCameraDirectionVector = function (camera, includeY) {
            if (camera === void 0) { camera = UI.game.scene.mainCamera; }
            if (includeY === void 0) { includeY = false; }
            var cosYRotation = Math.cos(camera.rotation.y);
            var sinYRotation = Math.sin(camera.rotation.y);
            return {
                x: cosYRotation + sinYRotation,
                z: cosYRotation - sinYRotation
            };
        };
        Utility.verticalCenterOffset = function (parentHeight, childHeight) {
            return (parentHeight - childHeight) / 2;
        };
        Utility.verticalCenterElement = function (element) {
            if (typeof element.data !== 'undefined' && element.data.element !== 'undefined')
                element = element.data.element;
            var offset = 0;
            var $el = $(element);
            if (typeof $el.attr('offset') != 'undefined')
                offset = parseInt($el.attr('offset'));
            element.style.marginTop = Utility.verticalCenterOffset($el.parent().height() - offset, $el.outerHeight()) + 'px';
        };
        Utility.verticalCenterElements = function () {
            $('.vertical-centered').each(function () {
                var t = $(this);
                if (this.tagName.toLowerCase() == 'img' && !t.data('loaded') && !t.data('vc-scheduled')) {
                    t.data('vc-scheduled', true).on('load', { element: this }, Utility.verticalCenterElement);
                    return;
                }
                Utility.verticalCenterElement(this);
            });
        };
        return Utility;
    })();
    UI_1.Utility = Utility;
})(UI || (UI = {}));
/// <reference path="UI.ts" />
/// <reference path="Character.ts" />
var Game;
(function (Game_1) {
    var Game = (function () {
        function Game(me) {
            this.title = "Game Test";
            this.currentNPCs = new Array();
            this.me = me;
            this.scene = new Game_1.Scene(this, me);
        }
        Game.startGame = function (me, name, callback) {
            if (me === void 0) { me = null; }
            if (name === void 0) { name = null; }
            if (callback === void 0) { callback = null; }
            if (!me) {
                if (!name)
                    name = "Guest";
                me = new Character.Me(name = name);
            }
            if (me)
                UI.UI.game = new Game(me);
            callback();
        };
        return Game;
    })();
    Game_1.Game = Game;
})(Game || (Game = {}));
/// <reference path="Game.ts" />
// <reference path="3d/materials/raw/babylon.waterMaterial.tsn" />
var Game;
(function (Game) {
    var SceneBase = (function () {
        function SceneBase(game, me) {
            var _this = this;
            if (game === void 0) { game = UI.UI.game; }
            if (me === void 0) { me = game.me; }
            this.scene = new BABYLON.Scene(UI.UI.engine);
            this.groundLevel = 0;
            this.extraGround = new Array();
            this.actions = {};
            this.animations = {};
            me.currentScene = this;
            me.moving = false;
            this.commonElements(me);
            UI.UI.setupGameBindings(game, this);
            setTimeout(function () {
                UI.UI.engine.hideLoadingUI();
                _this.scene.render();
            }, 1500);
        }
        SceneBase.prototype.startRendering = function () {
            var _this = this;
            UI.UI.engine.runRenderLoop(function () {
                if (!UI.UI.windowActive || UI.UI.mouse.y > 1)
                    return false;
                if ((_this.mainCamera.rotation.x < 1 && UI.UI.mouse.y < 0) ||
                    (_this.mainCamera.rotation.x > -1 && UI.UI.mouse.y > 0)) {
                    _this.mainCamera.rotation.x += (-UI.UI.mouse.y * Math.abs(UI.UI.mouse.y)) / 20;
                }
                _this.mainCamera.rotation.y += (UI.UI.mouse.x * (UI.UI.game.me.moving ? 1 : Math.abs(UI.UI.mouse.x))) / (UI.UI.game.me.moving ? 40 : 20);
                _this.scene.render();
            });
            return this;
        };
        SceneBase.prototype.commonElements = function (me) {
            if (me === void 0) { me = UI.UI.game.me; }
            BABYLON.SceneOptimizerOptions.HighDegradationAllowed(40);
            this.materials = new Game.MaterialLibrary(this);
            this.scene.clearColor = new BABYLON.Color3(0.5, 0.6, 0.75);
            this.scene.ambientColor = new BABYLON.Color3(0.5, 0.5, 0.5);
            this.scene.gravity = new BABYLON.Vector3(0, -10, 0);
            this.scene.fogMode = BABYLON.Scene.FOGMODE_EXP2;
            this.scene.fogEnabled = true;
            this.scene.fogDensity = 0.02;
            this.scene.fogColor.r = 0.5;
            this.scene.fogColor.g = 0.6;
            this.scene.fogColor.b = 0.9;
            this.mainCamera = new BABYLON.FreeCamera("mainCamera", new BABYLON.Vector3(me.coordinates.x, this.groundLevel + 4, me.coordinates.z), this.scene);
            this.mainCamera.maxZ = 256;
            this.mainCamera.ellipsoid = new BABYLON.Vector3(0.5, 1.5, 0.5);
            this.mainCamera.checkCollisions = true;
            this.mainCamera.applyGravity = true;
            me.camera = this.mainCamera;
            this.mainCamera.setEnabled(true);
            this.environmentBox = BABYLON.Mesh.CreateBox('environmentBox', 100, this.scene);
            this.environmentBox.infiniteDistance = true;
            return this;
        };
        SceneBase.prototype.setupShadows = function (objects, shadowTargets, light) {
            if (objects === void 0) { objects = this.objects; }
            if (shadowTargets === void 0) { shadowTargets = this.extraGround.concat(this.ground); }
            if (light === void 0) { light = this.sunlight; }
            if (objects == null)
                return;
            if (!this.shadowGenerator) {
                this.shadowGenerator = new BABYLON.ShadowGenerator(1024, light);
                this.shadowGenerator.useVarianceShadowMap = true;
                this.shadowGenerator.bias = 0.01;
                this.shadowGenerator.setDarkness(0.5);
            }
            var shadowGenerator = this.shadowGenerator;
            function recursiveAddToShadowGenerator(item, type) {
                console.log(item);
                if (!$.isArray(item) && !$.isPlainObject(item)) {
                    if (type == 'renderlist') {
                        shadowGenerator.getShadowMap().renderList.push(item);
                    }
                    if (type == 'shadowtargets') {
                        item.receiveShadows = true;
                    }
                    return;
                }
                else {
                    $.each(item, function (subObjkey, subObjVal) {
                        recursiveAddToShadowGenerator(item[subObjkey], type);
                    });
                }
            }
            recursiveAddToShadowGenerator(objects, 'renderlist');
            recursiveAddToShadowGenerator(shadowTargets, 'shadowtargets');
            return this;
        };
        SceneBase.prototype.destruct = function (placeholderText) {
            /* Ensure complete dereferencing to minimize risk of eventual memory overload */
            if (placeholderText === void 0) { placeholderText = 'Reconstructing Level'; }
            this.ground.dispose();
            this.mainCamera.dispose();
            this.sunlight.dispose();
            this.shadowGenerator.dispose();
            while (this.extraGround.length) {
                this.extraGround[0].dispose();
                delete this.extraGround.pop();
            }
            $.each(this.objects, function (i, o) { o.dispose(); });
            delete this.actions,
                this.animations,
                this.objects,
                this.extraGround,
                this.ground,
                this.mainCamera,
                this.sunlight,
                this.shadowGenerator,
                this.materials;
            this.scene.dispose();
            delete this.scene;
            UI.UI.engine.stopRenderLoop();
            UI.UI.engine.flushFramebuffer();
            UI.UI.engine.loadingUIText = placeholderText;
            UI.UI.engine.displayLoadingUI();
            delete this;
        };
        return SceneBase;
    })();
    Game.SceneBase = SceneBase;
    var Scene = (function (_super) {
        __extends(Scene, _super);
        function Scene(game, me) {
            if (game === void 0) { game = UI.UI.game; }
            if (me === void 0) { me = game.me; }
            _super.call(this, game, me);
            this.groundLevel = 2;
            this.setupEnvironment(me)
                .setupDefaultScene(me)
                .setupShadows([this.objects.sphere, this.objects.buildings], this.extraGround.concat(this.ground, this.objects.buildings))
                .startRendering();
        }
        Scene.prototype.setupEnvironment = function (me) {
            if (me === void 0) { me = UI.UI.game.me; }
            this.scene.enablePhysics(this.scene.gravity);
            this.scene.collisionsEnabled = true;
            this.sunlight = new BABYLON.SpotLight('sunlight', new BABYLON.Vector3(10, 256, 10), new BABYLON.Vector3(0, -10, 0), 120, 2, this.scene);
            this.sunlight.setDirectionToTarget(new BABYLON.Vector3(0, 0, 0));
            this.sunlight.position.y = 512;
            this.environmentBox.material = this.materials.environment.skybox;
            return this;
        };
        Scene.prototype.setupDefaultScene = function (me) {
            var _this = this;
            this.objects = {
                sphere: BABYLON.Mesh.CreateSphere('sphere1', 16, 4, this.scene),
                water: BABYLON.Mesh.CreateGround("water", 512, 512, 1, this.scene, true),
                waterGround: BABYLON.Mesh.CreateGround("waterGround", 512, 512, 1, this.scene),
                buildings: new Array(),
                meshCopies: null
            };
            this.ground = BABYLON.Mesh.CreateGround('ground', 258, 258, 1, this.scene, true);
            this.ground.position = new BABYLON.Vector3(0, 2, 144);
            this.ground.material = this.materials.environment.terrain.sandGrassConcrete;
            this.ground.setPhysicsState(BABYLON.PhysicsEngine.PlaneImpostor, { mass: 0, friction: 1, restitution: 0.4 });
            this.ground.checkCollisions = true;
            this.extraGround.push(BABYLON.Mesh.CreateGroundFromHeightMap("BottomBeach", "textures/heightmap/BeachMap_5x1.jpg", 320, 64, 64, -2, 2.01, this.scene));
            this.extraGround[0].position.z = -16;
            this.extraGround[0].position.x = 32;
            this.extraGround[0].material = this.materials.environment.sand5X1;
            this.extraGround[0].setPhysicsState(BABYLON.PhysicsEngine.HeightmapImpostor, { mass: 0, friction: 1, restitution: 1 });
            this.extraGround[0].checkCollisions = true;
            this.extraGround.push(BABYLON.Mesh.CreateGroundFromHeightMap("RightBeach", "textures/heightmap/BeachMap_1x4.jpg", 64, 256, 64, -2, 2.01, this.scene));
            this.extraGround[1].position.z = 144;
            this.extraGround[1].position.x = 160;
            this.extraGround[1].material = this.materials.environment.sand1X4;
            this.extraGround[1].checkCollisions = true;
            this.objects.waterGround.material = this.materials.environment.sand;
            this.objects.waterGround.position.y = -2;
            this.objects.sphere.position.y = 12;
            this.objects.sphere.position.z = 25;
            this.objects.sphere.material = this.materials.matte.concrete;
            this.objects.sphere.setPhysicsState(BABYLON.PhysicsEngine.SphereImpostor, { mass: 10, restitution: 1.75, friction: 0 });
            this.objects.sphere.checkCollisions = true;
            this.materials.environment.water.addToRenderList(this.environmentBox);
            this.materials.environment.water.addToRenderList(this.extraGround[0]);
            this.materials.environment.water.addToRenderList(this.extraGround[1]);
            this.materials.environment.water.addToRenderList(this.objects.waterGround);
            this.objects.water.material = this.materials.environment.water;
            BABYLON.SceneLoader.ImportMesh(["Building_Small_1"], "meshes/buildings/", "building.small.1.babylon", this.scene, function (meshes, particleSystems) {
                _this.objects.buildings = _this.objects.buildings.concat(meshes);
                meshes[0].position.x = -64.20;
                meshes[0].position.z = 142;
                meshes[0].position.y = _this.groundLevel;
                meshes[1].material = _this.materials.matte.redbrick;
                meshes[2].material = _this.materials.matte.concrete;
                meshes[3].material = _this.materials.matte.concrete;
                meshes[4].material = _this.materials.matte.concrete;
                meshes[5].material = _this.materials.matte.concrete;
                _this.objects.meshCopies = {
                    bldg_instances: [
                        [
                            meshes[0].createInstance('Building_Small_1_Instance_2'),
                            meshes[1].createInstance('Building_Walls_Instance_2'),
                            meshes[2].createInstance('WindowSills_Instance_2'),
                            meshes[3].createInstance('DoorAwning_Instance_2'),
                            meshes[4].createInstance('Building_Upper_Area_Instance_2'),
                            meshes[5].createInstance('Building_Foundation_Instance_2')
                        ],
                        [
                            meshes[0].createInstance('Building_Small_1_Instance_3'),
                            meshes[1].createInstance('Building_Walls_Instance_3'),
                            meshes[2].createInstance('WindowSills_Instance_3'),
                            meshes[3].createInstance('DoorAwning_Instance_3'),
                            meshes[4].createInstance('Building_Upper_Area_Instance_3'),
                            meshes[5].createInstance('Building_Foundation_Instance_3'),
                        ],
                        [
                            meshes[0].createInstance('Building_Small_1_Instance_4'),
                            meshes[1].createInstance('Building_Walls_Instance_4'),
                            meshes[2].createInstance('WindowSills_Instance_4'),
                            meshes[3].createInstance('DoorAwning_Instance_4'),
                            meshes[4].createInstance('Building_Upper_Area_Instance_4'),
                            meshes[5].createInstance('Building_Foundation_Instance_4'),
                        ]
                    ]
                };
                _this.objects.meshCopies.bldg_instances[0][2].parent =
                    _this.objects.meshCopies.bldg_instances[0][3].parent = _this.objects.meshCopies.bldg_instances[0][1];
                _this.objects.meshCopies.bldg_instances[0][1].parent =
                    _this.objects.meshCopies.bldg_instances[0][4].parent =
                        _this.objects.meshCopies.bldg_instances[0][5].parent = _this.objects.meshCopies.bldg_instances[0][0];
                _this.objects.meshCopies.bldg_instances[0][0].position = meshes[0].position.clone();
                _this.objects.meshCopies.bldg_instances[0][0].position.x = -78.5;
                _this.objects.meshCopies.bldg_instances[0][0].position.z = 140;
                _this.objects.meshCopies.bldg_instances[0][0].rotation.y = Math.PI / 2;
                _this.objects.meshCopies.bldg_instances[1][2].parent =
                    _this.objects.meshCopies.bldg_instances[1][3].parent = _this.objects.meshCopies.bldg_instances[1][1];
                _this.objects.meshCopies.bldg_instances[1][1].parent =
                    _this.objects.meshCopies.bldg_instances[1][4].parent =
                        _this.objects.meshCopies.bldg_instances[1][5].parent = _this.objects.meshCopies.bldg_instances[1][0];
                _this.objects.meshCopies.bldg_instances[1][0].position = _this.objects.meshCopies.bldg_instances[0][0].position.clone();
                _this.objects.meshCopies.bldg_instances[1][0].rotation = _this.objects.meshCopies.bldg_instances[0][0].rotation.clone();
                _this.objects.meshCopies.bldg_instances[1][0].position.z = 147.52;
                _this.objects.meshCopies.bldg_instances[1][0].position.x = -78.51;
                _this.objects.meshCopies.bldg_instances[2][2].parent =
                    _this.objects.meshCopies.bldg_instances[2][3].parent = _this.objects.meshCopies.bldg_instances[2][1];
                _this.objects.meshCopies.bldg_instances[2][1].parent =
                    _this.objects.meshCopies.bldg_instances[2][4].parent =
                        _this.objects.meshCopies.bldg_instances[2][5].parent = _this.objects.meshCopies.bldg_instances[2][0];
                _this.objects.meshCopies.bldg_instances[2][0].position = _this.objects.meshCopies.bldg_instances[1][0].position.clone();
                _this.objects.meshCopies.bldg_instances[2][0].rotation.y = Math.PI * 1.5;
                _this.objects.meshCopies.bldg_instances[2][0].position.z = 147.52;
                _this.objects.meshCopies.bldg_instances[2][0].position.x = -94.5;
                _this.setupShadows([meshes, _this.objects.meshCopies.bldg_instances], meshes);
            });
            return this;
        };
        return Scene;
    })(SceneBase);
    Game.Scene = Scene;
})(Game || (Game = {}));
var UI;
(function (UI) {
    var Storage = (function () {
        function Storage() {
        }
        return Storage;
    })();
    UI.Storage = Storage;
})(UI || (UI = {}));
/// <reference path="./classes/UI.ts" />
/// <reference path="./classes/Game.ts" />
/// <reference path="./classes/Character.ts" />
$(document).on('ready', function () {
    UI.UI.initialize();
    var nameInput = $('#name_input_field');
    $('#intro_form').one('submit', function (event) {
        event.preventDefault();
        var name = nameInput.val();
        if (typeof Storage !== 'undefined') {
            if (name) {
                localStorage.setItem('username', name);
            }
            else {
                localStorage.removeItem('username');
            }
        }
        Game.Game.startGame(null, name || "Guest", function () {
            setTimeout(function () {
                $('#intro_container').remove();
            }, 1500);
        });
        UI.UI.resizeCallback();
    });
    if (typeof Storage !== 'undefined')
        nameInput.val(localStorage.getItem('username'));
});
$(window).on('load', function () {
    UI.UI.documentBody.addClass('page-loaded');
    UI.UI.resizeCallback();
});
//# sourceMappingURL=app.js.map