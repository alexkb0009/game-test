﻿module UI {

    export class UI {

        /* DOM Elements */
        static canvasContainer  : JQuery;
        static canvas           : JQuery;
        static canvasRaw;
        static documentBody     : JQuery;
        static topBar           : JQuery;

        /* Babylon Stuff, etc. */

        static engine       : BABYLON.Engine;
        static mouse        : BABYLON.Vector2;
        static windowActive : boolean = true;

        /* State Variables */
        static currentState = {
            document: {
                width: 0,
                height: 0
            },
            window: {
                height: 0,
                heightOffset: 0,
                width: 0
            }
        };

        static controlsActive = {
            up:        false,
            left:      false,
            down:      false,
            right:     false,
            extraFast: false
        }
        
        static game: Game.Game;

        static DOMBinding = {
            documentBody : null,
        };

        static ActionBinding = {

        }


        static getCurrentControlsDirection() {
            return {
                x: UI.controlsActive.right ? 1 : (UI.controlsActive.left ? -1 : 0),
                z: UI.controlsActive.down ? -1 : (UI.controlsActive.up ? 1 : 0)
            }
        }

        /* Getter for (supposed-to-be) canvas height */
        static canvasHeight() {
            return UI.currentState.window.height - UI.currentState.window.heightOffset;
        }

        /* Initalizor */
        static initialize() {
            UI.canvasContainer = $("#canvas_container");
            UI.documentBody = $("body");
            UI.topBar = UI.documentBody.find('#top_bar');
            UI.updateSizeVariables();

            var canvasHTML = '<canvas id="game_canvas" width="' + UI.currentState.document.width + '" height="' + UI.canvasHeight() + '"></canvas>';

            UI.canvasContainer.html(canvasHTML);
            UI.canvasRaw = document.getElementById('game_canvas');
            UI.canvas = UI.canvasContainer.children('#game_canvas');
            UI.engine = new BABYLON.Engine(UI.canvasRaw);

            UI.resizeCallback(true);
            $(window).on('resize', () => {
                setTimeout(UI.resizeCallback, 150);
            });
            
        }

        static setupUIBindings(game: Game.Game) {

            /* Mouse Coords */

            UI.mouse = new BABYLON.Vector2(0, 0);
            UI.documentBody.on('mousemove', (event) => {
                UI.mouse.x = (event.clientX / UI.currentState.document.width) * 2 - 1;
                UI.mouse.y = - ((event.clientY - UI.currentState.window.heightOffset) / UI.canvasHeight()) * 2 + 1;
            });


            /* Set windowActive flag onBlur and onFocus */

            $(window).on('blur', () => { UI.windowActive = false; });
            $(window).on('focus', () => { UI.windowActive = true; });


            /* This is largely dependant on template */

            if (!UI.DOMBinding.documentBody) UI.DOMBinding.documentBody = rivets.bind(UI.documentBody, { game: game });
            
            $('.in-game-only').css('display', '');
            setTimeout(() => {
                UI.documentBody.addClass('ui-game-loaded');
            }, 250);

            $('#top_bar .resolution.menu > li > a').on('click', function() {

                var $this = $(this);
                switch ($this.text()) {

                    case 'High':
                        UI.engine.setHardwareScalingLevel(1);
                        break;

                    case 'Medium':
                        UI.engine.setHardwareScalingLevel(1.25);
                        break;

                    case 'Low':
                        UI.engine.setHardwareScalingLevel(1.5);
                        break;

                    case 'Very Low':
                        UI.engine.setHardwareScalingLevel(2);
                        break;

                    case '1992':
                        UI.engine.setHardwareScalingLevel(2.5);
                        break;

                    case '1989':
                        UI.engine.setHardwareScalingLevel(4);
                        break;

                }

                $this.parent().children('active').removeClass('active');
                $this.addClass('active');

            });

        }

        static setupGameBindings(game: Game.Game, scene: Game.SceneBase = game.scene) {

            scene.scene.actionManager = new BABYLON.ActionManager(scene.scene);

            scene.actions['keydown'] = new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, (event) => {

                /* Move character (+ camera) via WASD buttons */

                switch (event.sourceEvent.keyCode) {

                    case 87:
                        UI.controlsActive.up = true;
                        break;
                    case 65:
                        UI.controlsActive.left = true;
                        break;
                    case 83:
                        UI.controlsActive.down = true;
                        break;
                    case 68:
                        UI.controlsActive.right = true;
                        break;
                    case 82:
                        UI.controlsActive.extraFast = true;
                        break;
                }


                if (!game.me.moving) {
                    // Don't set a timeout b.c. delays execution.
                    // e.g. setTimeout($.proxy(game.me.move, game.me, null, null, UI.getCurrentControlsDirection(), true), 0);
                    game.me.move(null, null, UI.getCurrentControlsDirection(), true);
                }

            });

            scene.actions['keyup'] = new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, (event) => {

                switch (event.sourceEvent.keyCode) {

                    case 87: // Up    / W
                        UI.controlsActive.up = false;
                        break;
                    case 65: // Left  / A
                        UI.controlsActive.left = false;
                        break;
                    case 83: // Down  / S
                        UI.controlsActive.down = false;
                        break;
                    case 68: // Right / D
                        UI.controlsActive.right = false;
                        break;
                    case 82: // Extra Fast / R
                        UI.controlsActive.extraFast = false;
                        break;
                }

            });
            scene.scene.actionManager.registerAction(scene.actions['keydown']);
            scene.scene.actionManager.registerAction(scene.actions['keyup']);

            /* If UI has been initialized before, skip initializing non-scene-dependent components and bindings. */ 
            if (!UI.documentBody.hasClass('ui-game-loaded')) UI.setupUIBindings(game);

        }

        /* Functions for UI resizing */
        static updateSizeVariables() {
            UI.currentState.document.width = UI.documentBody.innerWidth();
            UI.currentState.document.height = UI.documentBody.innerHeight();
            UI.currentState.window.height = window.innerHeight;
            UI.currentState.window.width = window.innerWidth;
            UI.currentState.window.heightOffset = UI.topBar.outerHeight();
        }

        static resizeCallback(skipUpdateSizing: boolean = false) {
            if (!skipUpdateSizing) UI.updateSizeVariables();
            UI.canvas.height(UI.canvasHeight());
            UI.canvas.width(UI.currentState.window.width);
            UI.engine.resize();
            Utility.verticalCenterElements();
        }

    }
    
    export class Messager {

        static messages = new Array<{ type: ImportanceLevel, message: string, read: boolean }>();

    }

    enum ImportanceLevel {
        Error,
        Important,
        Minor,
        Player, 
        Status,
        Dialog,
        Debug
    }

    export class Utility {

        static getCameraDirectionVector(camera: BABYLON.FreeCamera = UI.game.scene.mainCamera, includeY: boolean = false) {

            var cosYRotation = Math.cos(camera.rotation.y);
            var sinYRotation = Math.sin(camera.rotation.y);

            /* ToDo: Implement Y axis direction */

            return {
                x: cosYRotation + sinYRotation,
                z: cosYRotation - sinYRotation
            };
        }

        static verticalCenterOffset(parentHeight: number, childHeight: number){
            return (parentHeight - childHeight) / 2;
        }

        static verticalCenterElement(element) {

            // For usage as event handler
            if (typeof element.data !== 'undefined' && element.data.element !== 'undefined') element = element.data.element;

            var offset = 0;
            var $el = $(element);

            if (typeof $el.attr('offset') != 'undefined') offset = parseInt($el.attr('offset'));

            element.style.marginTop = Utility.verticalCenterOffset(
                $el.parent().height() - offset,
                $el.outerHeight()
            ) + 'px';

        }

        static verticalCenterElements() {
            $('.vertical-centered').each(function () {
                var t = $(this);
                if (this.tagName.toLowerCase() == 'img' && !t.data('loaded') && !t.data('vc-scheduled')) {
                    t.data('vc-scheduled', true).on('load', { element: this }, Utility.verticalCenterElement);
                    return;
                }
                Utility.verticalCenterElement(this);
            });
        }

    }

}