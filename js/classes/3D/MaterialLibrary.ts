﻿module Game {

    /* Materials Library */

    export class MaterialLibrary {

        scene: SceneBase;

        metal: {
            steel: BABYLON.StandardMaterial;

        }

        matte: {
            concrete: BABYLON.StandardMaterial;
            redbrick: BABYLON.StandardMaterial;
        }

        environment: {
            skybox: BABYLON.StandardMaterial;
            water: any;
            sand: any;
            sand5X1: any;
            sand1X4: any;
            terrain: {
                sandGrassConcrete: BABYLON.TerrainMaterial;
            }
        }

        createMaterials() {

            /* Metals */
            this.metal = {
                steel: new BABYLON.StandardMaterial('steel_1', this.scene.scene)
            }
            this.metal.steel.diffuseColor = new BABYLON.Color3(0.75, 0.75, 0.75);
            this.metal.steel.specularColor = new BABYLON.Color3(1.5, 1.5, 1.5);


            /* Mattes */

            this.matte = {
                concrete: new BABYLON.StandardMaterial('concrete_1', this.scene.scene),
                redbrick: new BABYLON.StandardMaterial('redbrick_1', this.scene.scene)
            }
            this.matte.concrete.diffuseTexture = new BABYLON.Texture("./textures/concrete_1.jpg", this.scene.scene);
            this.matte.concrete.diffuseTexture.vScale = 1;
            this.matte.concrete.diffuseTexture.uScale = 1;
            this.matte.concrete.specularColor = new BABYLON.Color3(0.05, 0.05, 0.05);
            this.matte.concrete.ambientColor = new BABYLON.Color3(0.9, 0.9, 0.9);
            
            this.matte.redbrick.diffuseTexture = new BABYLON.Texture("./textures/redbrick_2.jpg", this.scene.scene);
            this.matte.redbrick.diffuseTexture.vScale = 1;
            this.matte.redbrick.diffuseTexture.uScale = 1;
            this.matte.redbrick.specularColor = new BABYLON.Color3(0,0,0);
            this.matte.redbrick.ambientColor = new BABYLON.Color3(1.2, 1.2, 1.2);
            //this.matte.concrete


            /* Environmental */
            this.environment = {
                skybox: new BABYLON.StandardMaterial('skybox', this.scene.scene),
                water: new BABYLON.WaterMaterial('water', this.scene.scene),
                sand: new BABYLON.StandardMaterial("sand", this.scene.scene),
                sand5X1: new BABYLON.StandardMaterial("sand5X1", this.scene.scene),
                sand1X4: new BABYLON.StandardMaterial("sand1X4", this.scene.scene),
                terrain: {
                    sandGrassConcrete: new BABYLON.TerrainMaterial("sandGrassConcrete1", this.scene.scene)
                }
            }

            /* Sky Config */

            this.environment.skybox.backFaceCulling = false;
            this.environment.skybox.diffuseColor = new BABYLON.Color3(0, 0, 0);
            this.environment.skybox.specularColor = new BABYLON.Color3(0, 0, 0);
            this.environment.skybox.reflectionTexture = new BABYLON.CubeTexture("./textures/skybox/skybox2", this.scene.scene);
            this.environment.skybox.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;

            /* Water Config */

            this.environment.water.backfaceCulling = true;
            this.environment.water.bumpTexture = new BABYLON.Texture("./textures/waterbump.png", this.scene.scene);
            this.environment.water.windForce = -15;
            this.environment.water.waveHeight = 0.25;
            this.environment.water.waveLength = 0.025;
            this.environment.water.waveSpeed = 0.4;
            this.environment.water.bumpHeight = 0.125;

            /* Sand Config */

            this.environment.sand.diffuseTexture = new BABYLON.Texture("textures/sand.jpg", this.scene.scene);
            this.environment.sand.diffuseTexture.vScale = 128;
            this.environment.sand.diffuseTexture.uScale = 128;

            this.environment.sand5X1.diffuseTexture = new BABYLON.Texture("textures/sand.jpg", this.scene.scene);
            this.environment.sand5X1.diffuseTexture.vScale = 32;
            this.environment.sand5X1.diffuseTexture.uScale = 160;

            /*
            this.environment.sand5X1.bumpTexture = new BABYLON.Texture("textures/sand_bump.jpg", this.scene.scene); 
            this.environment.sand5X1.bumpTexture.vScale = 8;
            this.environment.sand5X1.bumpTexture.uScale = 40;
            */

            //this.environment.sand5X1.bumpTexture.wAng = (Math.PI);
            this.environment.sand5X1.ambientColor = new BABYLON.Color3(1.8, 1.8, 1.8);
            /*
            this.environment.sand5X1.specularTexture = new BABYLON.Texture("textures/sand_spec.jpg", this.scene.scene); 
            this.environment.sand5X1.specularTexture.vScale = 16;
            this.environment.sand5X1.specularTexture.uScale = 80;
            */
            this.environment.sand1X4.diffuseTexture = new BABYLON.Texture("textures/sand.jpg", this.scene.scene);
            this.environment.sand1X4.diffuseTexture.vScale = 128;
            this.environment.sand1X4.diffuseTexture.uScale = 32;
            this.environment.sand1X4.ambientColor = new BABYLON.Color3(1.8, 1.8, 1.8)



            this.environment.sand.specularColor =
            this.environment.sand5X1.specularColor =
            this.environment.sand1X4.specularColor =
            new BABYLON.Color3(0.1, 0.1, 0.1);

            /* Terrain Config */

            this.environment.terrain.sandGrassConcrete.mixTexture = new BABYLON.Texture("textures/terrainmap/TerrainMap_1x1_BeachInner.png", this.scene.scene);

            this.environment.terrain.sandGrassConcrete.diffuseTexture1 = new BABYLON.Texture("textures/sand.jpg", this.scene.scene); // Red
            this.environment.terrain.sandGrassConcrete.diffuseTexture1.vScale = 128;
            this.environment.terrain.sandGrassConcrete.diffuseTexture1.uScale = 128;

            //this.environment.terrain.sandGrassConcrete.bumpTexture1 = new BABYLON.Texture("textures/sand_bump.jpg", this.scene.scene);
            //this.environment.terrain.sandGrassConcrete.bumpTexture1.vScale = 64;
            //this.environment.terrain.sandGrassConcrete.bumpTexture1.uScale = 64;
            // this.environment.terrain.sandGrassConcrete.bumpTexture1.wAng = (Math.PI);
            // this.environment.terrain.sandGrassConcrete.ambientColor = new BABYLON.Color3(1.7, 1.7, 1.7);


            this.environment.terrain.sandGrassConcrete.diffuseTexture2 = new BABYLON.Texture("textures/grass1_2.jpg", this.scene.scene); // Green
            //this.environment.terrain.sandGrassConcrete.bumpTexture2 = new BABYLON.Texture("textures/grass1_2.jpg", this.scene.scene); 
            this.environment.terrain.sandGrassConcrete.diffuseTexture2.vScale = 48;
            this.environment.terrain.sandGrassConcrete.diffuseTexture2.uScale = 48;

            this.environment.terrain.sandGrassConcrete.diffuseTexture3 = new BABYLON.Texture("textures/stoneTile_1.jpg", this.scene.scene); // Blue
            this.environment.terrain.sandGrassConcrete.diffuseTexture3.vScale = 64;
            this.environment.terrain.sandGrassConcrete.diffuseTexture3.uScale = 64;

            this.environment.terrain.sandGrassConcrete.specularColor = new BABYLON.Color3(0.1, 0.1, 0.1);
            this.environment.terrain.sandGrassConcrete.ambientColor = new BABYLON.Color3(1.8, 1.8, 1.8);
        }

        constructor(scene: SceneBase) {
            this.scene = scene;
            this.createMaterials();
        }


    }

}