﻿module Material {


    /* Mostly adapted from http://blogs.msdn.com/b/eternalcoding/archive/2013/08/06/babylon-js-creating-a-convincing-world-for-your-game-with-custom-shaders-height-maps-and-skyboxes.aspx */
    export class Water extends BABYLON.Material {

        name:   string;
        id: string;
        light: BABYLON.PointLight;

        bumpTexture: BABYLON.Texture;
        reflectionTexture: BABYLON.Texture;
        refractionTexture: BABYLON.Texture;

        actions = {};
        actionManager: BABYLON.ActionManager;

        constructor(name: string, scene: BABYLON.Scene, light) {
            super(name, scene);
            this.name = name;
            this.id = name;
            this.light = light;
            scene.materials.push(this);

            /* Bump Texture */
            this.bumpTexture = new BABYLON.Texture("", scene);
            this.bumpTexture.uScale = 2;
            this.bumpTexture.vScale = 2;
            this.bumpTexture.wrapU = BABYLON.Texture.MIRROR_ADDRESSMODE;
            this.bumpTexture.wrapV = BABYLON.Texture.MIRROR_ADDRESSMODE;
            
            /* Reflection */
            this.reflectionTexture = new BABYLON.MirrorTexture("reflection", 512, scene, true);
            this.reflectionTexture['mirrorPlane'] = new BABYLON.Plane(0, -1, 0, 0);

            /* Refraction */
            this.refractionTexture = new BABYLON.RenderTargetTexture("refraction", 512, scene, true); 

            this.getRenderTargetTextures = () => {
                var results = new BABYLON.SmartArray<BABYLON.RenderTargetTexture>(2);

                results.push(this.reflectionTexture);
                results.push(this.refractionTexture);

                return results;
            };

            BABYLON.Engine.ShadersRepository = "";

            //this.actionManager = new BABYLON.ActionManager(scene);
            //this.actions['beforeRender'] = new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnEveryFrameTrigger, (event) => { });


        }

        waterColor          = new BABYLON.Color3(0.0, 0.3, 0.1);
        waterColorLevel     = 0.2;
        fresnelLevel        = 1;
        reflectionLevel     = 0.6;
        refractionLevel     = 0.8;
        waveLength          = 0.1;
        waveHeight          = 0.15;
        waterDirection      = new BABYLON.Vector2(0, 1.0);
        _time = 0;
        
        isReady(mesh) {
            var engine = this.getScene().getEngine();
            
            if (this.bumpTexture && !this.bumpTexture.isReady()) {
                return false;
            }

            this._effect = engine.createEffect(
                "./classes/3D/Water",
                ["position", "normal", "uv"],
                ["worldViewProjection", "world", "view", "vLightPosition", "vEyePosition", "waterColor", "vLevels", "waveData", "windMatrix"],
                ["reflectionSampler", "refractionSampler", "bumpSampler"],
                ""
            );

            if (!this._effect.isReady()) {
                return false;
            }

        }

        bind(world, mesh) {
            this._time += 0.0001 * this.getScene().getAnimationRatio();

            this._effect.setMatrix("world", world);
            this._effect.setMatrix("worldViewProjection", world.multiply(this.getScene().getTransformMatrix()));
            this._effect.setVector3("vEyePosition", this.getScene().activeCamera.position);
            this._effect.setVector3("vLightPosition", this.light.position);
            this._effect.setColor3("waterColor", this.waterColor);
            this._effect.setFloat4("vLevels", this.waterColorLevel, this.fresnelLevel, this.reflectionLevel, this.refractionLevel);
            this._effect.setFloat2("waveData", this.waveLength, this.waveHeight);

            // Textures        
            this._effect.setMatrix(
                "windMatrix", this.bumpTexture.getTextureMatrix().multiply(
                    BABYLON.Matrix.Translation(this.waterDirection.x * this._time, this.waterDirection.y * this._time, 0)
                )
            );
            this._effect.setTexture("bumpSampler", this.bumpTexture);
            this._effect.setTexture("reflectionSampler", this.reflectionTexture);
            this._effect.setTexture("refractionSampler", this.refractionTexture);
        }


        needAlphaBlending() { return false; }

        needAlphaTesting() { return false; }
        
        dispose() {
            this.dispose();
            if (this.bumpTexture) {
                this.bumpTexture.dispose();
            }

            if (this.reflectionTexture) {
                this.reflectionTexture.dispose();
            }

            if (this.refractionTexture) {
                this.refractionTexture.dispose();
            }
        }

    }

}